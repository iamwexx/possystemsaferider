// javascript to check for strength of a password and alert the user as they enter.
// Reason:-> strong password reduces chances of hacking a profile.
// Author:-> Leakey Emmanuel.

function check_strength() { // function name
var strength = document.getElementById('strength'); // characters to check for during verification.
var very_strongRegex = new RegExp("^(?=.{9,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[$@_!%*#?&.,']).*$", "g"); // characters that must be contained in very strong password. 
var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*).*$", "g");  // characters that must be contained in strong password.
var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g");  // characters that must be contained in the medium strength.
var enoughRegex = new RegExp("(?=.{6,}).*", "g");  // a password should contain atleast 6 characters.
var pwd = document.getElementById("password");   // form element name.


if (pwd.value.length == 0) {	
document.getElementById('submit').disabled=false;					// if there is no password entered, strength is null.
strength.innerHTML = '';
} 
else if (very_strongRegex.test(pwd.value)) {
document.getElementById('submit').disabled=false;
strength.innerHTML = '<span style="color:green">Very Strong! </span>';	// alert if password is very strong
}
 else if (strongRegex.test(pwd.value)) {
 document.getElementById('submit').disabled=false;
strength.innerHTML = '<span style="color:green">Strong! </span>';	// alert if password is strong
} 
else if (mediumRegex.test(pwd.value)) {
document.getElementById('submit').disabled=false;
strength.innerHTML = '<span style="color:indigo">Medium! </span>';	// alert if password is medium strong
} 
else if(pwd.value.length <=5){
	document.getElementById('submit').disabled=true;
	strength.innerHTML = '<span style="color:red">Password Must not be less than 6 characters !</span>';
}
else {
document.getElementById('submit').disabled=false;
strength.innerHTML = '<span style="color:orange">Weak! </span>';		// alert if password is weak.
}
}

// just call this function where you need it and let it do the majic...........