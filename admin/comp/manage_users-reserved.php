<?php require_once 'comp/check_user.php'; // database connection.
	  require_once 'comp/statistics.php'; // dashboard startistics.
?>

<!DOCTYPE html>
<html>
<head>
	<title>Administrator: Main</title>
	<link rel="stylesheet" type="text/css" href="comp/main_style.css">
	<link rel="stylesheet" type="text/css" href="comp/design.css">
	<link rel="stylesheet" type="text/css" href="comp/font-awesome/font-awesome/font-awesome.css">


</head>



<body>
	<div id="top">
		<h1>UoK DigiBoard</h1>
		<p>Manage usage and send notices to everyone wherever they are.</p>
	</div>

<div class="main">
	<br>
	<div class="col-sm-2 left-design">
		<div id="top_left">
			<br><br><br>
			<p><?php echo ucwords ($user_fname); ?> &nbsp;&nbsp; <?php echo ucwords ($user_sname);?></p>
			<p><?php echo ucwords ($department); ?></p>
			<a href="../admin/verify/logout.php?logout" title="logout" style="color: red">logout</a>
		</div>

		<br><br>

		<a href="main.php"> <button class="but"><i class="fa fa-home al"></i> &nbsp;&nbsp; Dashboard</button></a>
		<a href="my_account.php"> <button class="but"><i class="fa fa-flag al"></i> &nbsp;&nbsp; My Account</button></a>
		<a href="add_user.php"> <button class="but"><i class="fa fa-user au"></i> &nbsp;&nbsp; Add Users</button></a>
		<a href="delete_user.php"> <button class="but"><i class="fa fa-trash du"></i> &nbsp;&nbsp; Delete Users</button></a>
		<a href="manage_users.php"> <button class="but btnactive"><i class="fa fa-tasks al"></i> &nbsp;&nbsp; Manage Users</button></a>
		<a href="#"> <button class="but"><i class="fa fa-envelope al"></i> &nbsp;&nbsp; Creat Notice</button></a>
		<a href="#"> <button class="but"><i class="fa fa-book al"></i> &nbsp;&nbsp; Manage Notices</button></a>
		<a href="#"> <button class="but"><i class="fa fa-comment al"></i> &nbsp;&nbsp; Manage comments</button></a>
		<a href="#"> <button class="but"><i class="fa fa-briefcase al"></i> &nbsp;&nbsp; Departments</button></a>
		<a href="#"> <button class="but"><i class="fa fa-leaf al"></i> &nbsp;&nbsp; UI guide</button></a>

<hr>
	</div>


	<div class="col-sm-10">

		<div style="margin-left: 20px">	
			<div id="dash">
				<h4> <i class="fa fa-tasks tn"></i> &nbsp;Manage Users</h4> 
			</div>

			<br>
			<div id="content">
				<div class="row">
					<div style="height: 12px"></div>

				<div class="col-sm-4">
					<div style="background-color: grey;width: 100%; height: 30px;text-align: center;"></div>
				<hr>
				<ul style="color: white">
					<li><p>To check user information, use Job ID to search for records. </p></li>
					<li><p>Current user information will be displayed.</p></li>
					<li><p>You can active/deactivate user account, to control his/her access to the system. </p></li>
				</ul>
				<br>
				<form method="post">
						<div class="search_user">
							<input type="text" class="form-control" name="user" placeholder="Enter Job ID" required>
							<span><button type="submit" name="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
						</div>
				</form>
				</div>

				<div class="col-sm-5" style="margin-left: 30px">
					<div style="background-color: grey;width: 100%; height: 30px;text-align: center;">Current User Details</div>
					<hr>

<div style="width: 250px;float: left;padding-left: 20px" id="up-var">
				<p>Names</p>
				<p>Gender</p>
				<p>Phone</p>
				<hr>
				<p>Job ID</p>
				<p>Department</p>
				<p>Role</p>
				<p>Status</p>
				<hr>
				<p>Reg. Date</p>
				<p>Last Login</p>
				<p>No. of Posts</p>
				
</div>				

<?php
if(isset($_POST['search'])){

$get_jobid = $_POST['user'];	// getting value from the form.

if($get_jobid ==""){
	$msg = "<div class='input-warning'> Please enter Job ID to search ! </div>";} // warning msg if author is not entered.

if(!isset($msg)){	// checks if all required filleds are inserted. 

		$stmt =$rec_db -> prepare("SELECT users.*, departments.depNAME FROM users, departments 
		WHERE users.depID = departments.depID AND jobID = '$get_jobid' ");
		$stmt ->execute(); 	
	
		if($res = $stmt->fetchAll(PDO::FETCH_ASSOC)){
		
	foreach ($res as $row){ 
		$name = $row['fname'];
		$sname = $row['sname'];
		$gender = $row['gender'];
		$phone = $row['phone'];
		$jobID = $row['jobID'];
		$department = $row['depNAME'];
		$role = $row['role'];
		$reg_date = $row['reg_date'];
		$last_login = $row['last_login'];
		$fname = $row['sname'];
		$status = $row['status'];

		if($status == 1){$user_status = 'Active';}else{$user_status = 'Locked';}
		if($role == 1){$user_role = 'Admin';}else{$user_role = 'Sec';}

		$stmt = $rec_db -> prepare("SELECT COUNT(*) AS num FROM texttable WHERE userID = '$jobID'");
    	$stmt ->execute();  // executes the query against the database.
     	if($res = $stmt->fetchAll(PDO::FETCH_ASSOC)){        // fetches all the values returned.
        foreach ($res as $row){
    	$user_posts = $row['num'];
    }
}
		
?>
			<div id="up-var2">
				<p>&nbsp; <?php echo $name; ?> <?php echo $sname; ?></p>
				<p>&nbsp; <?php echo $gender;  ?></p>
				<p>&nbsp; <?php echo $phone;  ?></p>
				<hr>
				<p>&nbsp; <?php echo $jobID;  ?></p>
				<p>&nbsp; <?php echo $department;  ?></p>
				<p>&nbsp; <?php echo $user_role;  ?></p>
				<p>&nbsp; <?php echo $user_status ?></p>
				<hr>
				<p>&nbsp; <?php echo $reg_date;  ?></p>
				<p>&nbsp; <?php echo $last_login;  ?></p>
				<p>&nbsp; <?php echo $user_posts;  ?></p>
			</div>
<?php 
	}
}
else{	// msg when book record does not exixt in db.
			$msg =  "<div class='alert alert-danger'> Sorry, unrecognized Job ID !</div>";
		}

}
else{ 
	//echo $msg;
	}
}
?>	
<div class="row">
	<div class="col-sm-12">
		<div style="height: 20px"></div>
	<?php
		if (isset($msg)) {
			echo $msg;
		}
		?>	
<div style="background-color0: red;height: 50px;width: 100%">
	<button style="width: 200px;margin-left: -15px;float: left;" class="btn btn-primary">Diactivate</button>
	<button style="width: 200px;margin-left: 55px;" class="btn btn-primary">Diactivate</button>
</div>

</div>
</div>
					</div>	
				</div>
			</div>	
		</div>					
	</div>
</div>
</body>
</html>