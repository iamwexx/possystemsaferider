<?php //require_once 'comp/check_user.php'; // database connection.
	 // require_once 'comp/statistics.php'; // dashboard startistics.
?>

<!DOCTYPE html>
<html>
<head>
	<title>Administrator: Add</title>
	<link rel="stylesheet" type="text/css" href="comp/main_style.css">
	<link rel="stylesheet" type="text/css" href="comp/design.css">
	<link rel="stylesheet" type="text/css" href="comp/font-awesome/font-awesome/font-awesome.css">
	<script src="../comp/js/jquery.min.js"></script>

</head>



<body>
	<div id="top">
		<h1>SAFERIDER MANAGEMENT SYSTEMS LTD</h1>
		<p>  </p>
	</div>

<div class="main">
	<br>
	<div class="col-sm-2 left-design">
		<div id="top_left">
			<br><br><br>
			<p> &nbsp;&nbsp; </p>
			<p></p>
			<a href="../admin/verify/logout.php?logout" title="logout" style="color: red">logout</a>
		</div>

		<br><br>

		<a href=""> <button class="but "><i class="fa fa-home al"></i> &nbsp;&nbsp; Dashboard</button></a>
		<a href=""> <button class="but btnactive"><i class="fa fa-flag al"></i> &nbsp;&nbsp; Products</button></a>
		<a href=""> <button class="but"><i class="fa fa-user au"></i> &nbsp;&nbsp; Sales</button></a>
		<a href=""> <button class="but"><i class="fa fa-tasks al"></i> &nbsp;&nbsp; Customers</button></a>
		<a href=""> <button class="but"><i class="fa fa-envelope al"></i> &nbsp;&nbsp; Payments</button></a>
		<a href=""> <button class="but"><i class="fa fa-book al"></i> &nbsp;&nbsp; Employees</button></a>
		<a href=""> <button class="but"><i class="fa fa-book al"></i> &nbsp;&nbsp; Reports</button></a>
		<a href=""> <button class="but"><i class="fa fa-briefcase al"></i> &nbsp;&nbsp; Settings</button></a>
		<a href=""> <button class="but"><i class="fa fa-leaf al"></i> &nbsp;&nbsp; UI guide</button></a>

<hr>
	</div>


	<div class="col-sm-10">

		<div style="margin-left: 20px">	
			<div id="dash">
				<h4> <i class="fa fa-pencil tn"></i> &nbsp;Add Items</h4> 
			</div>
 
			<br>

			<div id="content">
				<div class="#" style="padding-left: 0px">
					<div style="height: 12px"></div>

<div class="col-sm-2">
<a href="#" class="mybut mybut2">Add New</a>
<div id="mbr"></div>
<a href="#" class="mybut">View Records</a>
<div id="mbr"></div>
<a href="#" class="mybut">Fast Moving</a>
<div id="mbr"></div>
<a href="#" class="mybut mybut3">Barcodes</a>
</div>

					

				</div>
			</div>

		</div>	
	</div>




</div>

<script type="text/javascript">
	  // retreave data frm db using $.ajax() method
$(document).ready(function() {  
  
  ajax_call = function(){
    
    $.ajax({
      url: 'comp/visitor.php',
      type: 'POST',
        success: function(response){        
        $("#visitors").html(response); 
    }
    });
  };
  var interval = 1000;  // refresh data sending and retreaval every 2s.
  setInterval(ajax_call,interval);
});
</script>
</body>
</html>

