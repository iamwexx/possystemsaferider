 
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="refresh" content="3; url= public/index.php">
	<title>saferider ltd</title>
	<link rel="stylesheet" type="text/css" href="materialize/css/matlea.css5">
	<link rel="stylesheet" type="text/css" href="../font-awesome/font-awesome/font-awesome.min.css5">
  	<link rel="stylesheet" type="text/css" href="../frameworks/btcss/bootstrap.min.css">
	<script src="../frameworks/jquery.min.js"></script>
	<script src="materialize/js/materialize.min.js5"></script>

<style>body{background-color: #FFFFC6;}
body{
    background-image:url("191.jpg");
    background-repeat:no-repeat;
    background-size: cover;
    background-attachment: fixed;
    background-position: center center;
    box-shadow: 2px 2px 2px
    }

@font-face{
  font-family:myFont;
  src:url(sansation_light.woff);
}
  @font-face{
  font-family:myFont2;
  src:url(m-cosiva.ttf);
}
  @font-face{
  font-family:myF;
  src:url(tang.ttf);
}
</style>
</head>
<body>



<div style="box-shadow: 2px 2px 20px">


 <div style="font-size: 40px;text-align: center;padding-top: 60px;">
<svg height="600" width="1050">

   <defs>
    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
      <stop offset="0%"
      style="stop-color:rgb(102,255,255);stop-opacity:1" />
      <stop offset="40%"
      style="stop-color:rgb(51, 102, 255);stop-opacity:1" />
      <stop offset="60%"
      style="stop-color:rgb(51, 102, 255);stop-opacity:1" />
      <stop offset="100%"
      style="stop-color:rgb(102, 255, 255);stop-opacity:1" />
    </linearGradient>

    <linearGradient id="grad2" x1="0%" y1="0%" x2="100%" y2="0%">
      <stop offset="0%"
      style="stop-color:rgb(102,255,255);stop-opacity:1" />
      <stop offset="40%"
      style="stop-color:rgb(0, 0, 0);stop-opacity:1" />
      <stop offset="80%"
      style="stop-color:rgb(0, 0, 0);stop-opacity:1" />
      <stop offset="100%"
      style="stop-color:rgb(102, 255, 255);stop-opacity:1" />
    </linearGradient>

    <linearGradient id="grad3" x1="0%" y1="0%" x2="100%" y2="0%">
      <stop offset="0%"
      style="stop-color:gold;stop-opacity:1" />
    
      <stop offset="100%"
      style="stop-color:orange;stop-opacity:1" />
    </linearGradient>
    
  </defs>
<rect x="130" y="160" rx="0" ry="0" width="790" height="200" fill="url(#grad3)" />
<text fill="#000" font-size="45" font-family="myFont" font-weight="bolder" x="143" y="230">SAFERIDER MANAGEMENT SYSTEM</text>
<text fill="#000" font-size="42" font-family="myFont2" font-weight="bolder" x="280" y="300">Inventory Management System</text>

  <rect x="270" y="360" rx="0" ry="0" width="650" height="150" fill="url(#grad2)" />
    <rect x="220" y="360" rx="500" ry="80" width="650" height="150" fill="url(#grad2)" />
      <rect x="130" y="360" rx="0" ry="80" width="630" height="150" fill="url(#grad1)" />
  <text fill="#000" font-size="35" font-family="myF" font-weight="bolder" x="310" y="430">Designed and Developed by</text>
  <text fill="#000" font-size="45" font-family="monospace" font-weight="bolder" x="260" y="470">Leakey E.</text>
  <text fill="#ff0" font-size="15" font-family="monospace" font-weight="bolder" x="350" y="505">© Saferider 2019.</text>
<circle cx="820" cy="436" r="20" stroke="black" stroke-width="3" fill="red" />

<line x1="270" y1="320" x2="750" y2="320" style="stroke:rgb(255,0,0);stroke-width:4" />
<line x1="270" y1="330" x2="750" y2="330" style="stroke:rgb(255,0,0);stroke-width:4" />
<line x1="270" y1="340" x2="750" y2="340" style="stroke:rgb(255,0,0);stroke-width:4" />

<polygon points="750,342, 700,318, 750,318" style="fill:rgb(255,0,0);fill-rule:nonzero;" />
</svg>

  
</div>


</body>
</html>