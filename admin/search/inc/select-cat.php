<?php
// product by Leakey © 2017.

//$rec_db =new PDO('sqlite:../../../xxx/material.db'); 	// creating connection to sqlite database
require_once '../../dbcon.php'; // database connection.

	try{
		$stmt =$rec_db -> prepare("SELECT category FROM books");
		$stmt ->execute(); 	// executes the query against the database.
	
		if($res =$stmt->fetchAll(PDO::FETCH_ASSOC)){		// fetches all the values returned.
		
	foreach ($res as $row){ 
    echo "<option value = '".$row['category']."'>".$row['category']."</option>";
	} 
		}
		else{	// msg when book record does not exixt in db.
			echo "<div class='input-warning'> Sorry, no categories available ! </div>";
		}
		
	}
	catch (PDOExecption $e){		// show any error in the script.
		echo $e->getMessage();
	}
?>