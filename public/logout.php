<?php
session_start();			
// PHP Author -> Leakey Emmanuel

if (!isset($_SESSION['userSession'])) {		// if there is no session re-directs to login page.
	header("Location: main-index.php");
} else if (isset($_SESSION['userSession'])!="") {		// is there is session heads to home page.
	header("Location: main-page.php");
}

 if(isset($_GET['logout'])) {			// confirms if logout button as been pressed.
	session_destroy();					// then destroys the session completely then directs to login page.
	unset($_SESSION['userSession']);
	header("Location:logged-out.php");
}
