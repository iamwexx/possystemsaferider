﻿
// javascript to check if values entered by user in two password fields match.
// Author Name => Leakey Emmanuel.

function check_match()
{
//Store the password field objects into variables ...
var pass1 = document.getElementById( 'password' );
var pass2 = document.getElementById( 'password2' );

//Store the Confimation Message Object ...
var message = document.getElementById('confirmMessage' );
//Set the colors we will be using in alerting user...
var goodColor = "#66cc66" ;
var badColor = "#ff6666" ;
pass2.style.backgroundColor = "white"; 	// initial color of password2-field.

if(password2.value == ""){	// if password2 field is empty, don't give alert message.
	message.innerHTML = "";
}
//Compare the values in the password field and the confirmation field
// password field (pass1) must not be empty.
else if (password.value == password2.value & password.value !=""){
//The passwords match.
//Set the color to the good color and inform
//the user that they have entered the correct password
pass2.style.backgroundColor = goodColor;
message.style.color = goodColor;
message.innerHTML = "Passwords Match!"
}else{
//The passwords do not match.
//Set the color to the bad color and
//notify the user.
message.style.color = badColor;
message.innerHTML = " Make Sure Passwords Match!";
}
}
