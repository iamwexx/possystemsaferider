<?php
// NOTE: including PHP in this manner call for the PHP part to be the first on the script.
// PHP Author--> Leakey Emmanuel. >>> leakey92@gmail.com
// PDO and SQlite Login.

session_start();// session started here.

if (isset($_SESSION['userSession'])!="") {  //checks if there is any session already created then re-directs a user to main page.
  header("Location: main-page.php");
  exit;
}

////////#######################################################################////////
if (isset($_POST['submit'])) {
  
$myname =strip_tags($_POST['jobid']); // values from html form
$pass =strip_tags($_POST['upass']);
$name = strtoupper($myname);

//$rec_db =new PDO('sqlite:../xxx/my_users.db');   // creating connection to sqlite database
require_once '../dbcon.php'; // database connection.

if(!$rec_db){
  echo "database connection failed !";
}else{
  try{
    $stmt =$rec_db -> prepare("SELECT * FROM tb_users WHERE job_id='$name' AND password='$pass'");
    $stmt ->execute();  // executes the query against the database.
    
    
    if($res =$stmt->fetchAll(PDO::FETCH_ASSOC)){    // fetches all the values returned.
    
  foreach ($res as $row){
  $_SESSION['userSession'] = $row['id'];  // creating session id
  header("location:../libra/home.php");   // then redirect to this page.
}
    }
    else{
    header("location:index2.php"); // if details dont match direct to this page.
    }
    $rec_db=null;    // close the databse connection.
  }
  catch (PDOExecption $e){    // show any error in the script.
    echo $e->getMessage();
  }
}
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>try mat</title>
	<link rel="stylesheet" type="text/css" href="materialize/css/matlea.css">
  <link rel="stylesheet" type="text/css" href="../../frameworks/btcss/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../../frameworks/font-awesome/font-awesome/font-awesome.min.css">
	<script src="../../frameworks/jquery.min.js"></script>
	<script src="materialize/js/materialize.min.js"></script>



<style>
body{background-color: #FFFFC6;box-shadow: 2px 2px 20px}
.loader {
  border: 16px solid #FFFFFF;
  border-radius: 50%;
  border-top: 16px solid #990000;
  border-right: 16px solid #FFFFFF;
  border-bottom: 16px solid #990000;
  width: 120px;
  height: 120px;
  animation: spin 6s linear;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

#upperbar{
	width: 100%;
	text-align: center;
	height:60px;
	background:linear-gradient(to right,#FFFF80, blue,cyan, blue,#FFFF80);

}
i.sz {font-size: 1.5em;}
li:hover{background-color: teal;}
</style>
</head>
<body>
<div id="upperbar"><h4 style="font-size:40px;padding-top: 7px;"> University of Kabianga</h4></div>

<div class="container">
<!--===================================================================================================================================== -->
<div class="fixed-action-btn" style="top: 15px; right: 30px;">
<!-- Element Showed -->
  <a id="menu" class="btn-floating btn-large cyan waves-effect waves-light" onclick="$('.tap-target').tapTarget('open')"><i class="fa fa-copyright"></i></a>

  <!-- Tap Target Structure -->
  <div class="tap-target" data-activates="menu" style="background-color: #2DCBFF;">
    <div class="tap-target-content"  style="text-align: center;color: #fff;">
      
       <h3><span style="font-size: 14px;font-family:monospace"><u><i>Designed</u> and <u>Programmed</i></u> by:</span>
       <br>K'nam Leakey Emmanuel</h3> 
       <h3>|||||||||| 0702044410 ||||||||||</h3>
      
      <h5>
        Software Dev enthuasist. Am just in love with making stuff work.
        Very exciting and fresh !
        <br> _____________Am in this game_____________
      </h5>
  <br><br>
      <div style="float: right;">
      <h5><span style="font-family:monospace; "> |=====University of Kabianga=====|</span></h5>

      <br><br>
      <h5>&copy;<u> LeakeySoft 2017</u></h5>
      </div>
    </div>
  </div>
  </div>

  <!--===================================================================================================================================== -->
  <div class="fixed-action-btn toolbar">
    <a class="btn-floating btn-large cyan">
      <i class="fa fa-bars"></i>
    </a>
    <ul>
      <li class="waves-effect waves-light"><a href="help.htm" target="_blank"><i class="fa fa-question sz"> Help</i></a></li>
      <li class="waves-effect waves-light"><a href="ui.htm" target="_blank"><i class="fa fa-desktop sz"> Ui</i></a></li>
      <li class="waves-effect waves-light"><a href="support.htm" target="_blank"><i class="fa fa-support sz"> Support</i></a></li>
      <li class="waves-effect waves-light"><a href="line.htm" target="_blank"><i class="fa fa-certificate sz"> Licenses</i></a></li>
    </ul>
  </div>
<!--===================================================================================================================================== -->



<div style="top: 75px;left: 100px; position: absolute;">
<a href="index2.php"><div class="loader"></div></a>
</div>


<div style="padding-top: 300px;width:400px;float:right; ">
<div class="alert alert-danger" style="text-align: center;"> Invalid Username or Password !</div>

<form action="index2.php" method="post">
<div class="form-group">
<input type="text" name="jobid" class="form-control" placeholder="Job ID" required autofocus>
</div>
<div class="form-group">
<input type="password" name="upass" class="form-control" placeholder="Password" required>
</div>
<button type="submit" class="btn btn-block waves-effect waves-light" name="submit">Sign In</button>
</form>
<br><br><br><br><br><br>
</div>


</div>
</body>
</html>