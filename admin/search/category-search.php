
<!DOCTYPE html>
<html>
<head>
	<title>search by category</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="inc/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="inc/w3.css">
	<link rel="stylesheet" type="text/css" href="../comp/bootstrap/css/bootstrap.css">
	
	<link rel="stylesheet" type="text/css" href="inc/design-style.css">
	<link rel="stylesheet" type="text/css" href="../comp/font-awesome/font-awesome/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="inc/search-but.css">
	<script src="../jquery.min.js"></script>
	<script src="jquery-ui.js"></script>
	
</head>

<body>

<div id="upperbar"><h4 style="font-size:40px;padding-top: 7px;"> Saferider Management System Ltd</h4></div>
<div class="container-fluid">

	<br>
	
<div class="col-sm-3">
<div id="leftbar">
<div class="w3-card-16">
	<div id="topleft">
	<img src="../user_images/<?php //echo $row['userpic']; ?>" class="img-rounded" style="max-height:140px">
	<br><br>

		<b>Name:</b><?php //echo $row['fname']; ?> <?php //echo $row['lname']; ?><br>
       <b>Pos:</b> <?php //echo $row['position']; ?> <span style="float:right" ><a class="btn btn-danger" title="sign out" href="../../public/logout.php?logout"></a></span>
        
	</div>
	</div>
<span id="demo" style="padding-left:40px;text-shadow: 4px 4px 4px #000000; color:white">  </span>

<?php include 'inc/nav-but.php' ?>
<div class="w3-card-16"><br><hr></div>
<div id="last">Product by Leakey</div>
</div>
</div>

<div class="col-sm-10">
<div id="dash">
	<h4> <i class="fa fa-search"></i> Search Records</h4> 
	</div>
<br>
	<div id="mainp">
		<br>

<div class="row">

<div class="col-sm-2">
<a href="title-search.php" class="mybut2">Item Category</a>
<div id="mbr"></div>
<a href="author-search.php" class="mybut2 right">Item Name</a>
<div id="mbr"></div>
<a href="#" class="mybut down">Item code</a>
</div>

<div class="col-sm-10">
	<div id="dash">
	<h4> Search using Category</h4> 
	</div>

<div class="row">
		
<div class="col-sm-5">
<br>

  <form action="" method="post">

  	<div class="form-group">
  		<div class="input-group">
  			<select class="form-control" name="category">
    	<option value="">Select Category Here</option>
        <option value="Physics">S. Governor</option>
        <option value="Biology">Furniture</option>
        <option value="Chemistry">Stationary</option>
        <option value="Environmental">Fuel valve</option>
        <option value="Computing">Air Valve</option>
      </select>
  			<span class="input-group-btn"><button type="submit" name="search" class="btn btn-primary"><i class="fa fa-search"></i></button></span>
  		</div>
  	</div>
  </form>
 </div>

<div class="col-sm-7">
	<div id="search-res">

		<?php /*
if(isset($_POST['search'])){

 require_once '../../dbcon.php'; // database connection.

$get_cat = $_POST['category'];	// getting value from the form.

if($get_cat ==""){
	$msg = "<div class='input-warning'>
 Please select category to search ! </div>";} // warning msg if any category is not selected.

if(!isset($msg)){	// checks if all required filleds are inserted. 

	try{
		$stmt =$rec_db -> prepare("SELECT * FROM books WHERE category LIKE '%$get_cat%' ORDER BY b_name ASC");
		$stmt ->execute(); 	// executes the query against the database.
	
		if($res =$stmt->fetchAll(PDO::FETCH_ASSOC)){		// fetches all the values returned.
		
	foreach ($res as $row){ 
?>
<div class="result-box">
			<div class="book-title"><?php echo ucwords($row["b_name"])?></div>
			<div class="book-category">Category: <?php echo ucfirst($row["category"])?></div>
			<div class="book-author">Author: <?php echo ucwords($row["author"])?></div>
			<div class="book-publisher">Publisher: <?php echo ucfirst($row["publisher"])?></div>
			<div class="book-year">Year: <em><?php echo $row["year"]?></em></div>
			<div class="book-shelf"> Shelf No: <?php echo $row["shelf"]?></div>
			<div class="book-year">Book No: <b><?php echo $row["b_no"]?></b></div>
			<div class="book-availability">
	<?php if($row['avail']==1)
	{ ?>
		<p class="label label-success"><?php echo "Available";?></p>
		<?php }else{ ?>
		<p class="label label-danger"><?php echo "Unavailable";?></p>
		<?php	};	
		?>
		</div>
		</div>
	
<?php	

	} 
		}
		else{	// msg when book record does not exixt in db.
			echo "<div class='input-warning'> Sorry, unrecognized category name </div>";
		}
		$rec_db=null;		// close the database connection.
	}
	catch (PDOExecption $e){		// show any error in the script.
		echo $e->getMessage();
	}
	}else{ echo $msg;}

}
*/
?>
	</div>
</div> 

	</div>
</div>

</div>		

</div><!-- main panel ends here -->
</div>

</div>

</body>
</html>
 

<script>var myVar=setInterval(function(){myTimer()},1000);function myTimer(){var d = new Date();document.getElementById("demo").innerHTML=d.toLocaleTimeString();}</script>

