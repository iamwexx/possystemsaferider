<?php
// user registration script.
// PHP Author--> Leakey Emmanuel
if (isset($_POST['submit'])) {

$name = $_POST['uname']; // values from html form
$pass = $_POST['upass'];


$newdb = new PDO('sqlite:../xxx/my_users.db'); 	// creating connection to sqlite database

if(!$newdb){
	echo "database connection failed !";
}else{
	try{
		$stmt = $newdb -> prepare("INSERT INTO test_tb ('uname', 'upass')VALUES('$name', '$pass');");
		if($stmt -> execute())
		{
			//if values are inserted successfully, redirect to a given page.....
			header('location: login.php');  
		}else{ 
			$msg = "<div class='alert alert-danger'>		 
					 Unable to complete Registration !  
				</div>";
		}
		$newdb=null;		// close the databse connection.
	}
	catch (PDOExecption $e){
		echo $e->getMessage();
	}
}
}
?>


<!DOCTYPE html>
<html>
<head>
	<title>HannaR App</title>
	<meta name= "charset=utf-8">
	<meta name="Author" content="Leakey Emmanuel">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css_frameworks/btcss/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
<body>
<div class="container-fluid">
<br>
	<a class="btn btn-default" href="register.php">Refresh</a>

<h2><span  style="color:gold; font-family:Segoe UI Emoji">HannaR</span> is a record keeping app that can be used for all forms of inventory system.</h2>
<p>It has been built with flexibility in mind to bring about extendability and user 
friendliness. All the records will be kept safe within the app and only authorised user
will gain access to it</p>
<h4>To begin using it, first create your account and then login to the main page.</h4>

<hr>
<div class="jumbotron">
<?php if(isset($msg)){ echo $msg;} ?>
<form action="" method="post">

	<div class="form-group">
	Username: <input type="text" class="form-control" name="uname" Required>
	</div>
	<div class="form-group">
	Password: <input type="password" class="form-control" name="upass" Required>
	</div>

<div>
	<input type="submit" class="btn btn-default" name="submit" value="Register">  Already in the system <a href="login.php">Login Here</a> 
</div>
</form>
</div>
<hr>
<p> After registering, you will have the power of this app in your fingertip.</p>
<p> Enjoy! </p>

</div>
</body>
</html>